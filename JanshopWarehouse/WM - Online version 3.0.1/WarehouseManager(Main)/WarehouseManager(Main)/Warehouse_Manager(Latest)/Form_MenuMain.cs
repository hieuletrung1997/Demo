﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Warehouse_Manager_Latest_
{
    public partial class Form_MenuMain : Form
    {
        public Form_MenuMain()
        {
            InitializeComponent();
        }

        Bunifu.Framework.UI.Drag dr = new Bunifu.Framework.UI.Drag();

        private void btnMenu_Click(object sender, EventArgs e)
        {
            if (sideMenu.Width == 50)
            {
                sideMenu.Visible = false;
                sideMenu.Width = 260;
                animatorPnl.ShowSync(sideMenu);
                animatorLogo.ShowSync(logo);
            }
            else

            {
                animatorLogo.HideSync(logo);
                sideMenu.Visible = false;
                sideMenu.Width = 50;
                animatorPnl.ShowSync(sideMenu);
            }
        }

        private void btnClothes_Click(object sender, EventArgs e)
        {
            userControl_ClothesWarehouse1.Visible = true;
            userControl_ClothesWarehouse1.BringToFront();
        }

        private void btnSupplier_Click(object sender, EventArgs e)
        {
            userControl_Supplier1.Visible = true;
            userControl_Supplier1.BringToFront();
        }

        private void btnCustomer_Click(object sender, EventArgs e)
        {
            userControl_Customer1.Visible = true;
            userControl_Customer1.BringToFront();
        }


        private void btnAbout_Click(object sender, EventArgs e)
        {
            userControl_AboutUs1.Visible = false;
            userControl_AboutUs1.BringToFront();
            userControl_AboutUs1.hideControls();
            animatorAboutUs.ShowSync(userControl_AboutUs1);
            userControl_AboutUs1.animateTM();
        }

       

        private void btnStockReceiving_Click(object sender, EventArgs e)
        {
            userControl_StockReceiving1.Visible = true;
            userControl_StockReceiving1.BringToFront();
        }

        private void bunifuFlatButton1_Click(object sender, EventArgs e)
        {
            userControl_StockDelivering1.Visible = true;
            userControl_StockDelivering1.BringToFront();
        }
    }
}
