﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Warehouse_Manager_Latest_
{
    public partial class UserControl_AboutUs : UserControl
    {
        public UserControl_AboutUs()
        {
            InitializeComponent();
        }

        public void animateTM()
        {
            hideControls();
            animate.Start();
        }

        private void animate_Tick(object sender, EventArgs e)
        {
            animate.Stop();
            showControls();
        }

        void showControls()
        {
            foreach(Control item in this.Controls)
            {
                animator.ShowSync(item);
                Application.DoEvents();
            }
        }

        public void hideControls()
        {
            foreach(Control item in this.Controls)
            {
                item.Visible = false;
                Application.DoEvents();
            }
        }
    }
}
