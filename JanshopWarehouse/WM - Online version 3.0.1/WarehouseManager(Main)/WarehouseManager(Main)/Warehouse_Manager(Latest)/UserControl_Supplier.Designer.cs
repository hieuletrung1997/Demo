﻿namespace Warehouse_Manager_Latest_
{
    partial class UserControl_Supplier
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControl_Supplier));
            this.btnAddCustomer = new Bunifu.Framework.UI.BunifuThinButton2();
            this.dataSupplier = new System.Windows.Forms.DataGridView();
            this.MaNCC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NhaCC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DiaChi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTCoDinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTDD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnReload = new Bunifu.Framework.UI.BunifuThinButton2();
            this.lbSupplierMain = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataSupplier)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.ActiveBorderThickness = 1;
            this.btnAddCustomer.ActiveCornerRadius = 20;
            this.btnAddCustomer.ActiveFillColor = System.Drawing.Color.Black;
            this.btnAddCustomer.ActiveForecolor = System.Drawing.Color.Magenta;
            this.btnAddCustomer.ActiveLineColor = System.Drawing.Color.Magenta;
            this.btnAddCustomer.BackColor = System.Drawing.Color.Transparent;
            this.btnAddCustomer.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAddCustomer.BackgroundImage")));
            this.btnAddCustomer.ButtonText = "Add Supplier";
            this.btnAddCustomer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddCustomer.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddCustomer.ForeColor = System.Drawing.Color.Transparent;
            this.btnAddCustomer.IdleBorderThickness = 1;
            this.btnAddCustomer.IdleCornerRadius = 20;
            this.btnAddCustomer.IdleFillColor = System.Drawing.Color.Black;
            this.btnAddCustomer.IdleForecolor = System.Drawing.Color.White;
            this.btnAddCustomer.IdleLineColor = System.Drawing.Color.Yellow;
            this.btnAddCustomer.Location = new System.Drawing.Point(240, 654);
            this.btnAddCustomer.Margin = new System.Windows.Forms.Padding(5);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(337, 85);
            this.btnAddCustomer.TabIndex = 12;
            this.btnAddCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // dataSupplier
            // 
            this.dataSupplier.AllowUserToAddRows = false;
            this.dataSupplier.AllowUserToDeleteRows = false;
            this.dataSupplier.BackgroundColor = System.Drawing.Color.White;
            this.dataSupplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataSupplier.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaNCC,
            this.NhaCC,
            this.DiaChi,
            this.DTCoDinh,
            this.DTDD});
            this.dataSupplier.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataSupplier.Location = new System.Drawing.Point(0, 84);
            this.dataSupplier.Name = "dataSupplier";
            this.dataSupplier.ReadOnly = true;
            this.dataSupplier.RowTemplate.Height = 24;
            this.dataSupplier.Size = new System.Drawing.Size(1316, 513);
            this.dataSupplier.TabIndex = 11;
            // 
            // MaNCC
            // 
            this.MaNCC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MaNCC.DataPropertyName = "MaNCC";
            this.MaNCC.HeaderText = "Mã Nhà Cung Cấp";
            this.MaNCC.Name = "MaNCC";
            this.MaNCC.ReadOnly = true;
            // 
            // NhaCC
            // 
            this.NhaCC.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NhaCC.DataPropertyName = "NhaCC";
            this.NhaCC.HeaderText = "Nhà Cung Cấp";
            this.NhaCC.Name = "NhaCC";
            this.NhaCC.ReadOnly = true;
            // 
            // DiaChi
            // 
            this.DiaChi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DiaChi.DataPropertyName = "DiaChi";
            this.DiaChi.HeaderText = "Địa Chỉ";
            this.DiaChi.Name = "DiaChi";
            this.DiaChi.ReadOnly = true;
            // 
            // DTCoDinh
            // 
            this.DTCoDinh.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DTCoDinh.DataPropertyName = "DTCoDinh";
            this.DTCoDinh.HeaderText = "Điện Thoại Cố Định";
            this.DTCoDinh.Name = "DTCoDinh";
            this.DTCoDinh.ReadOnly = true;
            // 
            // DTDD
            // 
            this.DTDD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DTDD.DataPropertyName = "DTDD";
            this.DTDD.HeaderText = "Điện Thoại Di Động";
            this.DTDD.Name = "DTDD";
            this.DTDD.ReadOnly = true;
            // 
            // btnReload
            // 
            this.btnReload.ActiveBorderThickness = 1;
            this.btnReload.ActiveCornerRadius = 20;
            this.btnReload.ActiveFillColor = System.Drawing.Color.Black;
            this.btnReload.ActiveForecolor = System.Drawing.Color.Magenta;
            this.btnReload.ActiveLineColor = System.Drawing.Color.Magenta;
            this.btnReload.BackColor = System.Drawing.Color.Transparent;
            this.btnReload.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReload.BackgroundImage")));
            this.btnReload.ButtonText = "Reload";
            this.btnReload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReload.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReload.ForeColor = System.Drawing.Color.Transparent;
            this.btnReload.IdleBorderThickness = 1;
            this.btnReload.IdleCornerRadius = 20;
            this.btnReload.IdleFillColor = System.Drawing.Color.Black;
            this.btnReload.IdleForecolor = System.Drawing.Color.White;
            this.btnReload.IdleLineColor = System.Drawing.Color.Yellow;
            this.btnReload.Location = new System.Drawing.Point(636, 654);
            this.btnReload.Margin = new System.Windows.Forms.Padding(5);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(337, 85);
            this.btnReload.TabIndex = 13;
            this.btnReload.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // lbSupplierMain
            // 
            this.lbSupplierMain.BackColor = System.Drawing.Color.Transparent;
            this.lbSupplierMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbSupplierMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSupplierMain.ForeColor = System.Drawing.Color.Black;
            this.lbSupplierMain.Location = new System.Drawing.Point(0, 0);
            this.lbSupplierMain.Name = "lbSupplierMain";
            this.lbSupplierMain.Size = new System.Drawing.Size(1316, 84);
            this.lbSupplierMain.TabIndex = 10;
            this.lbSupplierMain.Text = "Supplier";
            this.lbSupplierMain.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // UserControl_Supplier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.btnAddCustomer);
            this.Controls.Add(this.dataSupplier);
            this.Controls.Add(this.lbSupplierMain);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "UserControl_Supplier";
            this.Size = new System.Drawing.Size(1316, 818);
            this.Load += new System.EventHandler(this.UserControl_Supplier_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSupplier)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuThinButton2 btnAddCustomer;
        private System.Windows.Forms.DataGridView dataSupplier;
        private Bunifu.Framework.UI.BunifuThinButton2 btnReload;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaNCC;
        private System.Windows.Forms.DataGridViewTextBoxColumn NhaCC;
        private System.Windows.Forms.DataGridViewTextBoxColumn DiaChi;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTCoDinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTDD;
        private System.Windows.Forms.Label lbSupplierMain;
    }
}
