﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Warehouse_Manager_Latest_
{
    public partial class UserControl_Customer : UserControl
    {
        public UserControl_Customer()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

        // bắt đầu viết hàm kết nối tới csdl
        private void KetNoiCSDL()
        {

            //mở kết nối trước:
            conn.Open();
            //tạo chuỗi kết nối:
            string sql = "SELECT *FROM dbo.KhachHang";//lấy hết dữ liệu trong bảng hocsinh
            SqlCommand cmd = new SqlCommand(sql, conn);// bắt đầu truy vấn bằng câu lệnh
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            // tạo một kho ảo để lưu trữ dữ liệu
            da.Fill(dt); //đổ dữ liệu vào kho
            //đóng kết nối
            conn.Close();
            //ta đổ dữ liệu vào trong gridDataView
            dataCustomer.DataSource = dt;
            this.dataCustomer.RowsDefaultCellStyle.BackColor = Color.Bisque;
            this.dataCustomer.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
        }
        public void HienThi()
        {

            string sqlSELECT = "SELECT *FROM KhachHang";
            SqlCommand cmd = new SqlCommand(sqlSELECT, conn);
            conn.Open();
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            dataCustomer.DataSource = dt;


        }

        public void Reload()
        {
            try
            {
                string sqlSELECT = "SELECT *FROM KhachHang";
                SqlCommand cmd = new SqlCommand(sqlSELECT, conn);
                SqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                dataCustomer.DataSource = dt;
            }
            catch
            {
                MessageBox.Show("Nothing changed");
            }

        }



        private void UserControl_Customer_Load_1(object sender, EventArgs e)
        {
            KetNoiCSDL();
            HienThi();
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            Form_Customer ss = new Form_Customer();
            ss.ShowDialog(this);
        }
    
        private void btnReload_Click_1(object sender, EventArgs e)
        {
            Reload();
        }
    }
}
