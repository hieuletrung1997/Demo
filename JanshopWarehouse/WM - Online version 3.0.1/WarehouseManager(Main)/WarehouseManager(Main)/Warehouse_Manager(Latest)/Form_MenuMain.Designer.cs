﻿namespace Warehouse_Manager_Latest_
{
    partial class Form_MenuMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation3 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation2 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_MenuMain));
            this.btnAbout = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnMenu = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnCustomer = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnSupplier = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnClothes = new Bunifu.Framework.UI.BunifuFlatButton();
            this.sideMenu = new System.Windows.Forms.Panel();
            this.logo = new System.Windows.Forms.PictureBox();
            this.btnStockReceiving = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pnlBoss = new System.Windows.Forms.Panel();
            this.userControl_AboutUs1 = new Warehouse_Manager_Latest_.UserControl_AboutUs();
            this.userControl_StockDelivering1 = new Warehouse_Manager_Latest_.UserControl_StockDelivering();
            this.userControl_StockReceiving1 = new Warehouse_Manager_Latest_.UserControl_StockReceiving();
            this.userControl_Customer1 = new Warehouse_Manager_Latest_.UserControl_Customer();
            this.userControl_Supplier1 = new Warehouse_Manager_Latest_.UserControl_Supplier();
            this.userControl_ClothesWarehouse1 = new Warehouse_Manager_Latest_.UserControl_ClothesWarehouse();
            this.animatorAboutUs = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.animatorLogo = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.animatorPnl = new BunifuAnimatorNS.BunifuTransition(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.btnMenu)).BeginInit();
            this.sideMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.pnlBoss.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAbout
            // 
            this.btnAbout.Activecolor = System.Drawing.Color.Purple;
            this.btnAbout.BackColor = System.Drawing.Color.Transparent;
            this.btnAbout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAbout.BorderRadius = 0;
            this.btnAbout.ButtonText = "                  About Us";
            this.btnAbout.Cursor = System.Windows.Forms.Cursors.Default;
            this.animatorPnl.SetDecoration(this.btnAbout, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.btnAbout, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.btnAbout, BunifuAnimatorNS.DecorationType.None);
            this.btnAbout.DisabledColor = System.Drawing.Color.Gray;
            this.btnAbout.ForeColor = System.Drawing.Color.White;
            this.btnAbout.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAbout.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAbout.Iconimage")));
            this.btnAbout.Iconimage_right = null;
            this.btnAbout.Iconimage_right_Selected = null;
            this.btnAbout.Iconimage_Selected = null;
            this.btnAbout.IconMarginLeft = 0;
            this.btnAbout.IconMarginRight = 0;
            this.btnAbout.IconRightVisible = true;
            this.btnAbout.IconRightZoom = 0D;
            this.btnAbout.IconVisible = true;
            this.btnAbout.IconZoom = 50D;
            this.btnAbout.IsTab = true;
            this.btnAbout.Location = new System.Drawing.Point(0, 715);
            this.btnAbout.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Normalcolor = System.Drawing.Color.Transparent;
            this.btnAbout.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnAbout.OnHoverTextColor = System.Drawing.Color.Lime;
            this.btnAbout.selected = false;
            this.btnAbout.Size = new System.Drawing.Size(335, 59);
            this.btnAbout.TabIndex = 22;
            this.btnAbout.Text = "                  About Us";
            this.btnAbout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAbout.Textcolor = System.Drawing.Color.White;
            this.btnAbout.TextFont = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // btnMenu
            // 
            this.btnMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMenu.BackColor = System.Drawing.Color.Transparent;
            this.animatorAboutUs.SetDecoration(this.btnMenu, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.btnMenu, BunifuAnimatorNS.DecorationType.None);
            this.animatorPnl.SetDecoration(this.btnMenu, BunifuAnimatorNS.DecorationType.None);
            this.btnMenu.Image = ((System.Drawing.Image)(resources.GetObject("btnMenu.Image")));
            this.btnMenu.ImageActive = null;
            this.btnMenu.Location = new System.Drawing.Point(269, 0);
            this.btnMenu.Name = "btnMenu";
            this.btnMenu.Size = new System.Drawing.Size(67, 75);
            this.btnMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMenu.TabIndex = 21;
            this.btnMenu.TabStop = false;
            this.btnMenu.Zoom = 10;
            this.btnMenu.Click += new System.EventHandler(this.btnMenu_Click);
            // 
            // btnCustomer
            // 
            this.btnCustomer.Activecolor = System.Drawing.Color.Purple;
            this.btnCustomer.BackColor = System.Drawing.Color.Transparent;
            this.btnCustomer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCustomer.BorderRadius = 0;
            this.btnCustomer.ButtonText = "                   Khách Hàng";
            this.btnCustomer.Cursor = System.Windows.Forms.Cursors.Default;
            this.animatorPnl.SetDecoration(this.btnCustomer, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.btnCustomer, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.btnCustomer, BunifuAnimatorNS.DecorationType.None);
            this.btnCustomer.DisabledColor = System.Drawing.Color.Gray;
            this.btnCustomer.Font = new System.Drawing.Font("Constantia", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnCustomer.ForeColor = System.Drawing.Color.White;
            this.btnCustomer.Iconcolor = System.Drawing.Color.Transparent;
            this.btnCustomer.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnCustomer.Iconimage")));
            this.btnCustomer.Iconimage_right = null;
            this.btnCustomer.Iconimage_right_Selected = null;
            this.btnCustomer.Iconimage_Selected = null;
            this.btnCustomer.IconMarginLeft = 0;
            this.btnCustomer.IconMarginRight = 0;
            this.btnCustomer.IconRightVisible = true;
            this.btnCustomer.IconRightZoom = 0D;
            this.btnCustomer.IconVisible = true;
            this.btnCustomer.IconZoom = 50D;
            this.btnCustomer.IsTab = true;
            this.btnCustomer.Location = new System.Drawing.Point(-1, 623);
            this.btnCustomer.Margin = new System.Windows.Forms.Padding(4);
            this.btnCustomer.Name = "btnCustomer";
            this.btnCustomer.Normalcolor = System.Drawing.Color.Transparent;
            this.btnCustomer.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnCustomer.OnHoverTextColor = System.Drawing.Color.Lime;
            this.btnCustomer.selected = false;
            this.btnCustomer.Size = new System.Drawing.Size(337, 59);
            this.btnCustomer.TabIndex = 19;
            this.btnCustomer.Text = "                   Khách Hàng";
            this.btnCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCustomer.Textcolor = System.Drawing.Color.White;
            this.btnCustomer.TextFont = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCustomer.Click += new System.EventHandler(this.btnCustomer_Click);
            // 
            // btnSupplier
            // 
            this.btnSupplier.Activecolor = System.Drawing.Color.Purple;
            this.btnSupplier.BackColor = System.Drawing.Color.Transparent;
            this.btnSupplier.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSupplier.BorderRadius = 0;
            this.btnSupplier.ButtonText = "                   Nhà Cung Cấp";
            this.btnSupplier.Cursor = System.Windows.Forms.Cursors.Default;
            this.animatorPnl.SetDecoration(this.btnSupplier, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.btnSupplier, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.btnSupplier, BunifuAnimatorNS.DecorationType.None);
            this.btnSupplier.DisabledColor = System.Drawing.Color.Gray;
            this.btnSupplier.Font = new System.Drawing.Font("Constantia", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnSupplier.ForeColor = System.Drawing.Color.White;
            this.btnSupplier.Iconcolor = System.Drawing.Color.Transparent;
            this.btnSupplier.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnSupplier.Iconimage")));
            this.btnSupplier.Iconimage_right = null;
            this.btnSupplier.Iconimage_right_Selected = null;
            this.btnSupplier.Iconimage_Selected = null;
            this.btnSupplier.IconMarginLeft = 0;
            this.btnSupplier.IconMarginRight = 0;
            this.btnSupplier.IconRightVisible = true;
            this.btnSupplier.IconRightZoom = 0D;
            this.btnSupplier.IconVisible = true;
            this.btnSupplier.IconZoom = 50D;
            this.btnSupplier.IsTab = true;
            this.btnSupplier.Location = new System.Drawing.Point(0, 531);
            this.btnSupplier.Margin = new System.Windows.Forms.Padding(4);
            this.btnSupplier.Name = "btnSupplier";
            this.btnSupplier.Normalcolor = System.Drawing.Color.Transparent;
            this.btnSupplier.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnSupplier.OnHoverTextColor = System.Drawing.Color.Lime;
            this.btnSupplier.selected = false;
            this.btnSupplier.Size = new System.Drawing.Size(335, 59);
            this.btnSupplier.TabIndex = 18;
            this.btnSupplier.Text = "                   Nhà Cung Cấp";
            this.btnSupplier.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSupplier.Textcolor = System.Drawing.Color.White;
            this.btnSupplier.TextFont = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSupplier.Click += new System.EventHandler(this.btnSupplier_Click);
            // 
            // btnClothes
            // 
            this.btnClothes.Activecolor = System.Drawing.Color.Purple;
            this.btnClothes.BackColor = System.Drawing.Color.Transparent;
            this.btnClothes.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnClothes.BorderRadius = 0;
            this.btnClothes.ButtonText = "                   Sản Phẩm";
            this.btnClothes.Cursor = System.Windows.Forms.Cursors.Default;
            this.animatorPnl.SetDecoration(this.btnClothes, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.btnClothes, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.btnClothes, BunifuAnimatorNS.DecorationType.None);
            this.btnClothes.DisabledColor = System.Drawing.Color.Gray;
            this.btnClothes.Font = new System.Drawing.Font("Constantia", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnClothes.ForeColor = System.Drawing.Color.White;
            this.btnClothes.Iconcolor = System.Drawing.Color.Transparent;
            this.btnClothes.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnClothes.Iconimage")));
            this.btnClothes.Iconimage_right = null;
            this.btnClothes.Iconimage_right_Selected = null;
            this.btnClothes.Iconimage_Selected = null;
            this.btnClothes.IconMarginLeft = 0;
            this.btnClothes.IconMarginRight = 0;
            this.btnClothes.IconRightVisible = true;
            this.btnClothes.IconRightZoom = 0D;
            this.btnClothes.IconVisible = true;
            this.btnClothes.IconZoom = 50D;
            this.btnClothes.IsTab = true;
            this.btnClothes.Location = new System.Drawing.Point(-1, 255);
            this.btnClothes.Margin = new System.Windows.Forms.Padding(4);
            this.btnClothes.Name = "btnClothes";
            this.btnClothes.Normalcolor = System.Drawing.Color.Transparent;
            this.btnClothes.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnClothes.OnHoverTextColor = System.Drawing.Color.Lime;
            this.btnClothes.selected = true;
            this.btnClothes.Size = new System.Drawing.Size(336, 59);
            this.btnClothes.TabIndex = 12;
            this.btnClothes.Text = "                   Sản Phẩm";
            this.btnClothes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnClothes.Textcolor = System.Drawing.Color.Transparent;
            this.btnClothes.TextFont = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClothes.Click += new System.EventHandler(this.btnClothes_Click);
            // 
            // sideMenu
            // 
            this.sideMenu.BackColor = System.Drawing.Color.Black;
            this.sideMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sideMenu.Controls.Add(this.logo);
            this.sideMenu.Controls.Add(this.btnStockReceiving);
            this.sideMenu.Controls.Add(this.bunifuFlatButton1);
            this.sideMenu.Controls.Add(this.btnAbout);
            this.sideMenu.Controls.Add(this.btnMenu);
            this.sideMenu.Controls.Add(this.btnCustomer);
            this.sideMenu.Controls.Add(this.btnSupplier);
            this.sideMenu.Controls.Add(this.btnClothes);
            this.animatorAboutUs.SetDecoration(this.sideMenu, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.sideMenu, BunifuAnimatorNS.DecorationType.None);
            this.animatorPnl.SetDecoration(this.sideMenu, BunifuAnimatorNS.DecorationType.None);
            this.sideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.sideMenu.Location = new System.Drawing.Point(0, 0);
            this.sideMenu.Name = "sideMenu";
            this.sideMenu.Size = new System.Drawing.Size(336, 818);
            this.sideMenu.TabIndex = 3;
            // 
            // logo
            // 
            this.logo.BackColor = System.Drawing.Color.Transparent;
            this.animatorPnl.SetDecoration(this.logo, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.logo, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.logo, BunifuAnimatorNS.DecorationType.None);
            this.logo.Image = ((System.Drawing.Image)(resources.GetObject("logo.Image")));
            this.logo.Location = new System.Drawing.Point(50, 59);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(220, 117);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logo.TabIndex = 25;
            this.logo.TabStop = false;
            // 
            // btnStockReceiving
            // 
            this.btnStockReceiving.Activecolor = System.Drawing.Color.Purple;
            this.btnStockReceiving.BackColor = System.Drawing.Color.Transparent;
            this.btnStockReceiving.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnStockReceiving.BorderRadius = 0;
            this.btnStockReceiving.ButtonText = "                  Nhập Kho";
            this.btnStockReceiving.Cursor = System.Windows.Forms.Cursors.Default;
            this.animatorPnl.SetDecoration(this.btnStockReceiving, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.btnStockReceiving, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.btnStockReceiving, BunifuAnimatorNS.DecorationType.None);
            this.btnStockReceiving.DisabledColor = System.Drawing.Color.Gray;
            this.btnStockReceiving.Font = new System.Drawing.Font("Constantia", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.btnStockReceiving.ForeColor = System.Drawing.Color.White;
            this.btnStockReceiving.Iconcolor = System.Drawing.Color.Transparent;
            this.btnStockReceiving.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnStockReceiving.Iconimage")));
            this.btnStockReceiving.Iconimage_right = null;
            this.btnStockReceiving.Iconimage_right_Selected = null;
            this.btnStockReceiving.Iconimage_Selected = null;
            this.btnStockReceiving.IconMarginLeft = 0;
            this.btnStockReceiving.IconMarginRight = 0;
            this.btnStockReceiving.IconRightVisible = true;
            this.btnStockReceiving.IconRightZoom = 0D;
            this.btnStockReceiving.IconVisible = true;
            this.btnStockReceiving.IconZoom = 50D;
            this.btnStockReceiving.IsTab = true;
            this.btnStockReceiving.Location = new System.Drawing.Point(4, 347);
            this.btnStockReceiving.Margin = new System.Windows.Forms.Padding(4);
            this.btnStockReceiving.Name = "btnStockReceiving";
            this.btnStockReceiving.Normalcolor = System.Drawing.Color.Transparent;
            this.btnStockReceiving.OnHovercolor = System.Drawing.Color.Transparent;
            this.btnStockReceiving.OnHoverTextColor = System.Drawing.Color.Lime;
            this.btnStockReceiving.selected = true;
            this.btnStockReceiving.Size = new System.Drawing.Size(331, 58);
            this.btnStockReceiving.TabIndex = 24;
            this.btnStockReceiving.Text = "                  Nhập Kho";
            this.btnStockReceiving.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStockReceiving.Textcolor = System.Drawing.Color.White;
            this.btnStockReceiving.TextFont = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStockReceiving.Click += new System.EventHandler(this.btnStockReceiving_Click);
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.Purple;
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "                   Xuất Kho";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Default;
            this.animatorPnl.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Font = new System.Drawing.Font("Constantia", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.bunifuFlatButton1.ForeColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton1.Iconimage")));
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 50D;
            this.bunifuFlatButton1.IsTab = true;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(-1, 439);
            this.bunifuFlatButton1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.Lime;
            this.bunifuFlatButton1.selected = true;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(336, 59);
            this.bunifuFlatButton1.TabIndex = 23;
            this.bunifuFlatButton1.Text = "                   Xuất Kho";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Century Gothic", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.bunifuFlatButton1_Click);
            // 
            // pnlBoss
            // 
            this.pnlBoss.BackColor = System.Drawing.Color.Black;
            this.pnlBoss.Controls.Add(this.userControl_AboutUs1);
            this.pnlBoss.Controls.Add(this.userControl_StockDelivering1);
            this.pnlBoss.Controls.Add(this.userControl_StockReceiving1);
            this.pnlBoss.Controls.Add(this.userControl_Customer1);
            this.pnlBoss.Controls.Add(this.userControl_Supplier1);
            this.pnlBoss.Controls.Add(this.userControl_ClothesWarehouse1);
            this.animatorAboutUs.SetDecoration(this.pnlBoss, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.pnlBoss, BunifuAnimatorNS.DecorationType.None);
            this.animatorPnl.SetDecoration(this.pnlBoss, BunifuAnimatorNS.DecorationType.None);
            this.pnlBoss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBoss.Location = new System.Drawing.Point(336, 0);
            this.pnlBoss.Name = "pnlBoss";
            this.pnlBoss.Size = new System.Drawing.Size(1316, 818);
            this.pnlBoss.TabIndex = 5;
            // 
            // userControl_AboutUs1
            // 
            this.userControl_AboutUs1.BackColor = System.Drawing.Color.Transparent;
            this.userControl_AboutUs1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl_AboutUs1.BackgroundImage")));
            this.userControl_AboutUs1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.animatorPnl.SetDecoration(this.userControl_AboutUs1, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.userControl_AboutUs1, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.userControl_AboutUs1, BunifuAnimatorNS.DecorationType.None);
            this.userControl_AboutUs1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControl_AboutUs1.Location = new System.Drawing.Point(0, 0);
            this.userControl_AboutUs1.Name = "userControl_AboutUs1";
            this.userControl_AboutUs1.Size = new System.Drawing.Size(1316, 818);
            this.userControl_AboutUs1.TabIndex = 11;
            // 
            // userControl_StockDelivering1
            // 
            this.userControl_StockDelivering1.BackColor = System.Drawing.Color.Transparent;
            this.userControl_StockDelivering1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl_StockDelivering1.BackgroundImage")));
            this.userControl_StockDelivering1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.animatorPnl.SetDecoration(this.userControl_StockDelivering1, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.userControl_StockDelivering1, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.userControl_StockDelivering1, BunifuAnimatorNS.DecorationType.None);
            this.userControl_StockDelivering1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControl_StockDelivering1.Location = new System.Drawing.Point(0, 0);
            this.userControl_StockDelivering1.Name = "userControl_StockDelivering1";
            this.userControl_StockDelivering1.Size = new System.Drawing.Size(1316, 818);
            this.userControl_StockDelivering1.TabIndex = 10;
            // 
            // userControl_StockReceiving1
            // 
            this.userControl_StockReceiving1.BackColor = System.Drawing.Color.Transparent;
            this.userControl_StockReceiving1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl_StockReceiving1.BackgroundImage")));
            this.userControl_StockReceiving1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.animatorPnl.SetDecoration(this.userControl_StockReceiving1, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.userControl_StockReceiving1, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.userControl_StockReceiving1, BunifuAnimatorNS.DecorationType.None);
            this.userControl_StockReceiving1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControl_StockReceiving1.Location = new System.Drawing.Point(0, 0);
            this.userControl_StockReceiving1.Name = "userControl_StockReceiving1";
            this.userControl_StockReceiving1.Size = new System.Drawing.Size(1316, 818);
            this.userControl_StockReceiving1.TabIndex = 9;
            // 
            // userControl_Customer1
            // 
            this.userControl_Customer1.BackColor = System.Drawing.Color.Transparent;
            this.userControl_Customer1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl_Customer1.BackgroundImage")));
            this.animatorPnl.SetDecoration(this.userControl_Customer1, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.userControl_Customer1, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.userControl_Customer1, BunifuAnimatorNS.DecorationType.None);
            this.userControl_Customer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControl_Customer1.Location = new System.Drawing.Point(0, 0);
            this.userControl_Customer1.Name = "userControl_Customer1";
            this.userControl_Customer1.Size = new System.Drawing.Size(1316, 818);
            this.userControl_Customer1.TabIndex = 8;
            // 
            // userControl_Supplier1
            // 
            this.userControl_Supplier1.BackColor = System.Drawing.Color.Transparent;
            this.userControl_Supplier1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl_Supplier1.BackgroundImage")));
            this.userControl_Supplier1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.animatorPnl.SetDecoration(this.userControl_Supplier1, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.userControl_Supplier1, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.userControl_Supplier1, BunifuAnimatorNS.DecorationType.None);
            this.userControl_Supplier1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControl_Supplier1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.userControl_Supplier1.Location = new System.Drawing.Point(0, 0);
            this.userControl_Supplier1.Name = "userControl_Supplier1";
            this.userControl_Supplier1.Size = new System.Drawing.Size(1316, 818);
            this.userControl_Supplier1.TabIndex = 7;
            // 
            // userControl_ClothesWarehouse1
            // 
            this.userControl_ClothesWarehouse1.BackColor = System.Drawing.Color.Transparent;
            this.userControl_ClothesWarehouse1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl_ClothesWarehouse1.BackgroundImage")));
            this.userControl_ClothesWarehouse1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.animatorPnl.SetDecoration(this.userControl_ClothesWarehouse1, BunifuAnimatorNS.DecorationType.None);
            this.animatorLogo.SetDecoration(this.userControl_ClothesWarehouse1, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this.userControl_ClothesWarehouse1, BunifuAnimatorNS.DecorationType.None);
            this.userControl_ClothesWarehouse1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.userControl_ClothesWarehouse1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.userControl_ClothesWarehouse1.Location = new System.Drawing.Point(0, 0);
            this.userControl_ClothesWarehouse1.Name = "userControl_ClothesWarehouse1";
            this.userControl_ClothesWarehouse1.Size = new System.Drawing.Size(1316, 818);
            this.userControl_ClothesWarehouse1.TabIndex = 6;
            // 
            // animatorAboutUs
            // 
            this.animatorAboutUs.AnimationType = BunifuAnimatorNS.AnimationType.Mosaic;
            this.animatorAboutUs.Cursor = null;
            animation3.AnimateOnlyDifferences = true;
            animation3.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.BlindCoeff")));
            animation3.LeafCoeff = 0F;
            animation3.MaxTime = 1F;
            animation3.MinTime = 0F;
            animation3.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicCoeff")));
            animation3.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicShift")));
            animation3.MosaicSize = 20;
            animation3.Padding = new System.Windows.Forms.Padding(30);
            animation3.RotateCoeff = 0F;
            animation3.RotateLimit = 0F;
            animation3.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.ScaleCoeff")));
            animation3.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.SlideCoeff")));
            animation3.TimeCoeff = 0F;
            animation3.TransparencyCoeff = 0F;
            this.animatorAboutUs.DefaultAnimation = animation3;
            // 
            // animatorLogo
            // 
            this.animatorLogo.AnimationType = BunifuAnimatorNS.AnimationType.Scale;
            this.animatorLogo.Cursor = null;
            animation2.AnimateOnlyDifferences = true;
            animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
            animation2.LeafCoeff = 0F;
            animation2.MaxTime = 1F;
            animation2.MinTime = 0F;
            animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
            animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
            animation2.MosaicSize = 0;
            animation2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            animation2.RotateCoeff = 0F;
            animation2.RotateLimit = 0F;
            animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
            animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
            animation2.TimeCoeff = 0F;
            animation2.TransparencyCoeff = 0F;
            this.animatorLogo.DefaultAnimation = animation2;
            // 
            // animatorPnl
            // 
            this.animatorPnl.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.animatorPnl.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.animatorPnl.DefaultAnimation = animation1;
            this.animatorPnl.TimeStep = 0.01F;
            // 
            // Form_MenuMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(1652, 818);
            this.Controls.Add(this.pnlBoss);
            this.Controls.Add(this.sideMenu);
            this.animatorLogo.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.animatorAboutUs.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.animatorPnl.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form_MenuMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Warehouse Manager";
            ((System.ComponentModel.ISupportInitialize)(this.btnMenu)).EndInit();
            this.sideMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.pnlBoss.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuFlatButton btnAbout;
        private Bunifu.Framework.UI.BunifuImageButton btnMenu;
        private Bunifu.Framework.UI.BunifuFlatButton btnCustomer;
        private Bunifu.Framework.UI.BunifuFlatButton btnSupplier;
        private Bunifu.Framework.UI.BunifuFlatButton btnClothes;
        private System.Windows.Forms.Panel sideMenu;
        private System.Windows.Forms.Panel pnlBoss;
        private BunifuAnimatorNS.BunifuTransition animatorAboutUs;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private System.Windows.Forms.PictureBox logo;
        private UserControl_ClothesWarehouse userControl_ClothesWarehouse1;
        private UserControl_Customer userControl_Customer1;
        private UserControl_Supplier userControl_Supplier1;
        private UserControl_StockReceiving userControl_StockReceiving1;
        private UserControl_StockDelivering userControl_StockDelivering1;
        private UserControl_AboutUs userControl_AboutUs1;
        private Bunifu.Framework.UI.BunifuFlatButton btnStockReceiving;
        private BunifuAnimatorNS.BunifuTransition animatorLogo;
        private BunifuAnimatorNS.BunifuTransition animatorPnl;
    }
}