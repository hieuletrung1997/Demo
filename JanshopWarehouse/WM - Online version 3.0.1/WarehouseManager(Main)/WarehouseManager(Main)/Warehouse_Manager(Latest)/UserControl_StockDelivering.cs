﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Warehouse_Manager_Latest_
{
    public partial class UserControl_StockDelivering : UserControl
    {
        public UserControl_StockDelivering()
        {
            InitializeComponent();
        }


        SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

        // bắt đầu viết hàm kết nối tới csdl
        private void KetNoiCSDL()
        {
            try
            {
                //mở kết nối trước:
                conn.Open();
                //tạo chuỗi kết nối:
                string sql = "SELECT *FROM HangHoa";//lấy hết dữ liệu trong bảng hocsinh
                SqlCommand cmd = new SqlCommand(sql, conn);// bắt đầu truy vấn bằng câu lệnh
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                // tạo một kho ảo để lưu trữ dữ liệu
                da.Fill(dt); //đổ dữ liệu vào kho
                             //đóng kết nối
                conn.Close();
                //ta đổ dữ liệu vào trong gridDataView
                dataStockDelivering.DataSource = dt;
                this.dataStockDelivering.RowsDefaultCellStyle.BackColor = Color.Bisque;
                this.dataStockDelivering.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
            }
            catch
            {
                MessageBox.Show("Lỗi kết nối, vui lòng kiểm tra lại");
            }

            finally
            {
                SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
                conn.Close(); //đóng kết nối lại

            }
        }


        public void HienThi()
        {
            string sqlSELECT = "SELECT *FROM HangHoa";
            SqlCommand cmd = new SqlCommand(sqlSELECT, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            dataStockDelivering.DataSource = dt;

        }


        private void loadData()
        {
            txtMaHang.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtMaHang.DataBindings.Add("Text", dataStockDelivering.DataSource, "MaHang");
            txtTenHang.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtTenHang.DataBindings.Add("Text", dataStockDelivering.DataSource, "TenHang");
            cbDonVi.DataBindings.Clear();//xóa dữ liệu trong textbox
            cbDonVi.DataBindings.Add("Text", dataStockDelivering.DataSource, "DonVi");
            txtSize.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtSize.DataBindings.Add("Text", dataStockDelivering.DataSource, "Size");
            txtSoLuong.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtSoLuong.DataBindings.Add("Text", dataStockDelivering.DataSource, "SoLuong");
            txtDonGia.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtDonGia.DataBindings.Add("Text", dataStockDelivering.DataSource, "DonGia");
            txtGiaBanLe.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtGiaBanLe.DataBindings.Add("Text", dataStockDelivering.DataSource, "GiaXuatBanLe");
            txtGiaBanSi.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtGiaBanSi.DataBindings.Add("Text", dataStockDelivering.DataSource, "GiaXuatBanSi");
            cbMaNCC.DataBindings.Clear();//xóa dữ liệu trong textbox
            cbMaNCC.DataBindings.Add("Text", dataStockDelivering.DataSource, "MaNCC");
            txtMoTa.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtMoTa.DataBindings.Add("Text", dataStockDelivering.DataSource, "MoTa");

        }

        public void layMaNCC()
        {
            KetnoiDatabase kn = new KetnoiDatabase();
            SqlCommand mysqlcommand = new SqlCommand();
            mysqlcommand.Connection = kn.kn;
            mysqlcommand.CommandText = "select nhaCC from NhaCungCap";
            kn.kn_csdl();
            SqlDataAdapter mysqladatareader = new SqlDataAdapter();
            mysqladatareader.SelectCommand = mysqlcommand;
            DataSet mydataset = new DataSet();
            mysqladatareader.Fill(mydataset, "KQ");
            DataTable table_MK = new DataTable();
            table_MK = mydataset.Tables["KQ"];
            cbMaNCC.DataSource = table_MK;
            cbMaNCC.DisplayMember = table_MK.Columns["NhaCC"].ToString();
            kn.kn.Close();

        }

        public static void clearTxt(Control ctrl)
        {
            if (ctrl is TextBox)
            {
                ctrl.Text = string.Empty;
            }
            foreach (Control i in ctrl.Controls)
            {
                clearTxt(i);
            }
        }
       


        private void btnReload_Click(object sender, EventArgs e)
        {
            conn.Open();
            HienThi();
            conn.Close();
            loadData();
        }

        private void UserControl_StockDelivering_Load(object sender, EventArgs e)
        {
            KetNoiCSDL();
            loadData();
            layMaNCC();
        }

        private void dataStockReceiving_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                dataStockDelivering.CurrentRow.Selected = true; // dữ liệu đc chọn cả dòng
            }

            catch
            {
            }
        }

     

        private void btnXuatHang_Click(object sender, EventArgs e)
        {
            DialogResult thongBao;
            thongBao = MessageBox.Show("Are You Sure?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
            if (thongBao == DialogResult.Yes)
            {
                try
                {
                    int soLuong = int.Parse(txtAddSoLuong.Text); // lấy thông tin số lượng từ ô textbox

                    string sqlEdit = "UPDATE HangHoa SET TenHang = @TenHang, DonVi = @DonVi,Size = @Size,SoLuong -= '" + txtAddSoLuong.Text + "' ,DonGia = @DonGia, GiaXuatBanLe = @GiaXuatBanLe, GiaXuatBanSi = @GiaXuatBanSi, MaNCC = @MaNCC, MoTa = @MoTa WHERE MaHang = @MaHang";
                    SqlCommand cmd = new SqlCommand(sqlEdit, conn);
                    cmd.Parameters.AddWithValue("MaHang", txtMaHang.Text);
                    cmd.Parameters.AddWithValue("TenHang", txtTenHang.Text);
                    cmd.Parameters.AddWithValue("DonVi", cbDonVi.Text);
                    cmd.Parameters.AddWithValue("Size", txtSize.Text);
                    cmd.Parameters.AddWithValue("SoLuong", txtAddSoLuong.Text);
                    cmd.Parameters.AddWithValue("DonGia", txtDonGia.Text);
                    cmd.Parameters.AddWithValue("GiaXuatBanLe", txtGiaBanLe.Text);
                    cmd.Parameters.AddWithValue("GiaXuatBanSi", txtGiaBanSi.Text);
                    cmd.Parameters.AddWithValue("MaNCC", cbMaNCC.Text);
                    cmd.Parameters.AddWithValue("MoTa", txtMoTa.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    HienThi();
                    conn.Close();
                    loadData();

                    MessageBox.Show("Đã Xuất thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch
                {
                    MessageBox.Show("Kiểu dữ liệu nhập không hợp lệ, xin nhập lại!", "Thông báo");
                    clearTxt(txtAddSoLuong);
                }
            }
        }
    }
}
