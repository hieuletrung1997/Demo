﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Warehouse_Manager_Latest_
{
    public partial class Form_Customer : Form
    {
        public Form_Customer()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
        // bắt đầu viết hàm kết nối tới csdl
        private void KetNoiCSDL()
        {
            //mở kết nối trước:
            conn.Open();
            //tạo chuỗi kết nối:
            string sql = "SELECT *FROM KhachHang";//lấy hết dữ liệu trong bảng hocsinh
            SqlCommand cmd = new SqlCommand(sql, conn);// bắt đầu truy vấn bằng câu lệnh
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            // tạo một kho ảo để lưu trữ dữ liệu
            da.Fill(dt); //đổ dữ liệu vào kho
            //đóng kết nối
            conn.Close();
            //ta đổ dữ liệu vào trong gridDataView
            dataCustomer.DataSource = dt;
            this.dataCustomer.RowsDefaultCellStyle.BackColor = Color.Bisque;
            this.dataCustomer.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
        }
        public void HienThi()
        {


            string sqlSELECT = "SELECT *FROM KhachHang";
            SqlCommand cmd = new SqlCommand(sqlSELECT, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            dataCustomer.DataSource = dt;
            conn.Close();

        }
        public static void clearTxt(Control ctrl)
        {
            if (ctrl is TextBox)
            {
                ctrl.Text = string.Empty;
            }
            foreach (Control i in ctrl.Controls)
            {
                clearTxt(i);
            }
        }

        private void loadData()
        {
            txtMaKH.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtMaKH.DataBindings.Add("Text", dataCustomer.DataSource, "MaKH");
            txtTenKH.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtTenKH.DataBindings.Add("Text", dataCustomer.DataSource, "TenKH");
            txtSDT.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtSDT.DataBindings.Add("Text", dataCustomer.DataSource, "SDT");
            txtDiaChi.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtDiaChi.DataBindings.Add("Text", dataCustomer.DataSource, "DiaChi");
            txtEmail.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtEmail.DataBindings.Add("Text", dataCustomer.DataSource, "Email");
            txtSoTK.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtSoTK.DataBindings.Add("Text", dataCustomer.DataSource, "SoTK");
        }


        private void btnNhapKho_Click(object sender, EventArgs e)
        {
            if (txtMaKH.Text == "")
            {
                baoLoi.SetError(txtMaKH, "Bạn chưa nhập mã KH");
            }
            if (txtTenKH.Text == "")
            {
                baoLoi.SetError(txtTenKH, "Bạn chưa nhập tên KH");
            }
            if (txtSDT.Text == "")
            {
                baoLoi.SetError(txtSDT, "Bạn chưa nhập số đt");
            }
            if (txtMaKH.Text != "" && txtTenKH.Text != "" && txtSDT.Text != "")

                try
                {
                    string sqlAdd = "Insert INTO KhachHang VALUES (@MaKH, @TenKH, @SDT, @DiaChi, @Email, @SoTK)";
                    SqlCommand cmd = new SqlCommand(sqlAdd, conn);
                    cmd.Parameters.AddWithValue("MaKH", txtMaKH.Text);
                    cmd.Parameters.AddWithValue("TenKH", txtTenKH.Text);
                    cmd.Parameters.AddWithValue("SDT", txtSDT.Text);
                    cmd.Parameters.AddWithValue("DiaChi", txtDiaChi.Text);
                    cmd.Parameters.AddWithValue("Email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("SoTK", txtSoTK.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    HienThi();
                    conn.Close();
                    MessageBox.Show("Đã nhập thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch (Exception ex)
                {
                    if (ex.GetType() == typeof(SqlException))
                    {
                        if (ex.Message.Contains("PRIMARY KEY"))
                        {
                            MessageBox.Show("Bạn đã nhập mã trùng với mã NCC, xin vui lòng nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    else
                    {
                        string sqlAdd = "Insert INTO KhachHang VALUES (@MaKH, @TenKH, @SDT, @DiaChi, @Email, @SoTK)";
                        SqlCommand cmd = new SqlCommand(sqlAdd, conn);
                        cmd.Parameters.AddWithValue("MaKH", txtMaKH.Text);
                        cmd.Parameters.AddWithValue("TenKH", txtTenKH.Text);
                        cmd.Parameters.AddWithValue("SDT", txtSDT.Text);
                        cmd.Parameters.AddWithValue("DiaChi", txtDiaChi.Text);
                        cmd.Parameters.AddWithValue("Email", txtEmail.Text);
                        cmd.Parameters.AddWithValue("SoTK", txtSoTK.Text);
                        cmd.ExecuteNonQuery();
                        HienThi();
                        conn.Close();

                        MessageBox.Show("Đã nhập thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    }
                }
                finally
                {
                    SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
                    conn.Close(); //đóng kết nối lại

                }
        }


        private void btnSua_Click(object sender, EventArgs e)
        {
            DialogResult thongBao;
            thongBao = MessageBox.Show("Are You Sure?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
            if (thongBao == DialogResult.Yes)
            {
                try
                {
                    string sqlEdit = "UPDATE KhachHang SET TenKH = @TenKH, SDT = @SDT, DiaChi = @DiaChi, Email = @Email, SoTk = @SoTk WHERE MaKH = @MaKH";
                    SqlCommand cmd = new SqlCommand(sqlEdit, conn);
                    cmd.Parameters.AddWithValue("MaKH", txtMaKH.Text);
                    cmd.Parameters.AddWithValue("TenKH", txtTenKH.Text);
                    cmd.Parameters.AddWithValue("SDT", txtSDT.Text);
                    cmd.Parameters.AddWithValue("DiaChi", txtDiaChi.Text);
                    cmd.Parameters.AddWithValue("Email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("SoTk", txtSoTK.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    HienThi();
                    conn.Close();
                    loadData();

                    MessageBox.Show("Đã sửa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch
                {
                    MessageBox.Show("Nothing changed, cant save!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult thongBao;
            thongBao = MessageBox.Show("Are You Sure?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
            if (thongBao == DialogResult.Yes)
            {
                try
                {

                    string sqlDELETE = "DELETE FROM KhachHang Where MaKH = @MaKH";
                    SqlCommand cmd = new SqlCommand(sqlDELETE, conn);
                    cmd.Parameters.AddWithValue("MaKH", txtMaKH.Text);
                    cmd.Parameters.AddWithValue("TenKH", txtTenKH.Text);
                    cmd.Parameters.AddWithValue("SDT", txtSDT.Text);
                    cmd.Parameters.AddWithValue("DiaChi", txtDiaChi.Text);
                    cmd.Parameters.AddWithValue("Email", txtEmail.Text);
                    cmd.Parameters.AddWithValue("SoTk", txtSoTK.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    HienThi();
                    conn.Close();
                    loadData();

                    MessageBox.Show("Đã xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch
                {
                    MessageBox.Show("Something went wrong");
                }
            }

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearTxt(this);
        }

        private void dataCustomer_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                dataCustomer.CurrentRow.Selected = true; // dữ liệu đc chọn cả dòng
            }

            catch
            {
            }
        }

        private void Form_Customer_Load(object sender, EventArgs e)
        {
            KetNoiCSDL();
            loadData();
        }

        private void txtMaKH_TextChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(txtMaKH,"");
        }

        private void txtTenKH_TextChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(txtTenKH, "");
        }

        private void txtSDT_TextChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(txtSDT, "");
        }
    }

}