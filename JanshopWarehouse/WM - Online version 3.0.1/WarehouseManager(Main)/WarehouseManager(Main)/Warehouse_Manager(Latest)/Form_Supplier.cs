﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Warehouse_Manager_Latest_
{
    public partial class Form_Supplier : Form
    {
        public Form_Supplier()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

        // bắt đầu viết hàm kết nối tới csdl
        private void KetNoiCSDL()
        {
            //mở kết nối trước:
            conn.Open();
            //tạo chuỗi kết nối:
            string sql = "SELECT *FROM NhaCungCap";//lấy hết dữ liệu trong bảng hocsinh
            SqlCommand cmd = new SqlCommand(sql, conn);// bắt đầu truy vấn bằng câu lệnh
            cmd.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            // tạo một kho ảo để lưu trữ dữ liệu
            da.Fill(dt); //đổ dữ liệu vào kho
            //đóng kết nối
            conn.Close();
            //ta đổ dữ liệu vào trong gridDataView
            dataSupplier.DataSource = dt;
            this.dataSupplier.RowsDefaultCellStyle.BackColor = Color.Bisque;
            this.dataSupplier.AlternatingRowsDefaultCellStyle.BackColor = Color.Beige;
        }
        public void HienThi()
        {


            string sqlSELECT = "SELECT *FROM NhaCungCap";
            SqlCommand cmd = new SqlCommand(sqlSELECT, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            dataSupplier.DataSource = dt;
            conn.Close();

        }

        public static void clearTxt(Control ctrl)
        {
            if (ctrl is TextBox)
            {
                ctrl.Text = string.Empty;
            }
            foreach (Control i in ctrl.Controls)
            {
                clearTxt(i);
            }
        }

        private void loadData()
        {
            txtMaNCC.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtMaNCC.DataBindings.Add("Text", dataSupplier.DataSource, "MaNCC");
            txtTenNCC.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtTenNCC.DataBindings.Add("Text", dataSupplier.DataSource, "NhaCC");
            txtDiaChiNCC.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtDiaChiNCC.DataBindings.Add("Text", dataSupplier.DataSource, "DiaChi");
            txtDTCD.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtDTCD.DataBindings.Add("Text", dataSupplier.DataSource, "DTCoDinh");
            txtDTDD.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtDTDD.DataBindings.Add("Text", dataSupplier.DataSource, "DTDD");

        }

        private void btnNhapKho_Click(object sender, EventArgs e)
        {
            if (txtMaNCC.Text == "")
            {
                baoLoi.SetError(txtMaNCC, "Bạn chưa nhập mã KH");
            }
            if (txtTenNCC.Text == "")
            {
                baoLoi.SetError(txtTenNCC, "Bạn chưa nhập tên KH");
            }
            if (txtDTDD.Text == "")
            {
                baoLoi.SetError(txtDTDD, "Bạn chưa nhập số đt");
            }
            if (txtMaNCC.Text != "" && txtTenNCC.Text != "" && txtDTDD.Text != "")
                try
                {
                    string sqlAdd = "Insert INTO NhaCungCap VALUES (@MaNCC, @NhaCC, @DiaChi, @DTCoDinh, @DTDD)";
                    SqlCommand cmd = new SqlCommand(sqlAdd, conn);
                    cmd.Parameters.AddWithValue("MaNCC", txtMaNCC.Text);
                    cmd.Parameters.AddWithValue("NhaCC", txtTenNCC.Text);
                    cmd.Parameters.AddWithValue("DiaChi", txtDiaChiNCC.Text);
                    cmd.Parameters.AddWithValue("DTCoDinh", txtDTCD.Text);
                    cmd.Parameters.AddWithValue("DTDD", txtDTDD.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    HienThi();
                    conn.Close();

                    MessageBox.Show("Đã nhập thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }

                catch (Exception ex)
                {
                    if (ex.GetType() == typeof(SqlException))
                    {
                        if (ex.Message.Contains("PRIMARY KEY"))
                        {
                            MessageBox.Show("Bạn đã nhập mã trùng với mã NCC, xin vui lòng nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    else
                    {
                        string sqlAdd = "Insert INTO NhaCungCap VALUES (@MaNCC, @NhaCC, @DiaChi, @DTCoDinh, @DTDD)";
                        SqlCommand cmd = new SqlCommand(sqlAdd, conn);
                        cmd.Parameters.AddWithValue("MaNCC", txtMaNCC.Text);
                        cmd.Parameters.AddWithValue("NhaCC", txtTenNCC.Text);
                        cmd.Parameters.AddWithValue("DiaChi", txtDiaChiNCC.Text);
                        cmd.Parameters.AddWithValue("DTCoDinh", txtDTCD.Text);
                        cmd.Parameters.AddWithValue("DTDD", txtDTDD.Text);
                        cmd.ExecuteNonQuery();
                        HienThi();
                        conn.Close();

                        MessageBox.Show("Đã nhập thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        //throw ex;
                    }
                }
                finally
                {
                    SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
                    conn.Close(); //đóng kết nối lại

                }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            DialogResult thongBao;
            thongBao = MessageBox.Show("Are You Sure?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
            if (thongBao == DialogResult.Yes)
            {

                try
                {
                    string sqlEdit = "UPDATE NhaCungCap SET NhaCC = @NhaCC, DiaChi = @DiaChi, DTCoDinh = @DTCoDinh, DTDD = @DTDD WHERE MaNCC = @MaNCC";
                    SqlCommand cmd = new SqlCommand(sqlEdit, conn);
                    cmd.Parameters.AddWithValue("MaNCC", txtMaNCC.Text);
                    cmd.Parameters.AddWithValue("NhaCC", txtTenNCC.Text);
                    cmd.Parameters.AddWithValue("DiaChi", txtDiaChiNCC.Text);
                    cmd.Parameters.AddWithValue("DTCoDinh", txtDTCD.Text);
                    cmd.Parameters.AddWithValue("DTDD", txtDTDD.Text);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    HienThi();
                    conn.Close();
                    loadData();

                    MessageBox.Show("Đã sửa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch
                {
                    MessageBox.Show("Nothing changed, cant save!", "Thông Báo", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult thongBao;
            thongBao = MessageBox.Show("Are You Sure?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
            if (thongBao == DialogResult.Yes)
            {
                try
                {
                    string sqlDELETE = "DELETE FROM NhaCungCap Where MaNCC = @MaNCC";
                    SqlCommand cmd = new SqlCommand(sqlDELETE, conn);
                    cmd.Parameters.AddWithValue("MaNCC", txtMaNCC.Text);
                    cmd.Parameters.AddWithValue("NhaCC", txtTenNCC.Text);
                    cmd.Parameters.AddWithValue("DiaChi", txtDiaChiNCC.Text);
                    cmd.Parameters.AddWithValue("DTCoDinh", txtDTCD.Text);
                    cmd.Parameters.AddWithValue("DTDD", txtDTDD.Text);

                    conn.Open();
                    cmd.ExecuteNonQuery();
                    HienThi();
                    conn.Close();
                    loadData();

                    MessageBox.Show("Đã xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch
                {
                    MessageBox.Show("Something went wrong");
                }
            }
        }

        private void Form_Supplier_Load(object sender, EventArgs e)
        {
            KetNoiCSDL();
            loadData();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearTxt(this);
        }

        private void dataSupplier_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                dataSupplier.CurrentRow.Selected = true; // dữ liệu đc chọn cả dòng
            }

            catch
            {
            }
        }

        private void txtDTDD_TextChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(txtDTDD, "");
        }

        private void txtTenNCC_TextChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(txtTenNCC, "");
        }

        private void txtMaNCC_TextChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(txtMaNCC, "");
        }
    }
}
