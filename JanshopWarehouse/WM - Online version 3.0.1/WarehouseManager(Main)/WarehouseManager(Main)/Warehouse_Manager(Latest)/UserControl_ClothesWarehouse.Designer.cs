﻿namespace Warehouse_Manager_Latest_
{
    partial class UserControl_ClothesWarehouse
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserControl_ClothesWarehouse));
            this.dataClothes = new System.Windows.Forms.DataGridView();
            this.MaHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenHang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DonVi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoLuong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.size = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DonGia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GiaXuatBanLe = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GiaXuatBanSi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MaNCC = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MoTa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNhapKho = new Bunifu.Framework.UI.BunifuThinButton2();
            this.btnReload = new Bunifu.Framework.UI.BunifuThinButton2();
            ((System.ComponentModel.ISupportInitialize)(this.dataClothes)).BeginInit();
            this.SuspendLayout();
            // 
            // dataClothes
            // 
            this.dataClothes.AllowUserToAddRows = false;
            this.dataClothes.AllowUserToDeleteRows = false;
            this.dataClothes.BackgroundColor = System.Drawing.Color.White;
            this.dataClothes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataClothes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaHang,
            this.TenHang,
            this.DonVi,
            this.SoLuong,
            this.size,
            this.DonGia,
            this.GiaXuatBanLe,
            this.GiaXuatBanSi,
            this.MaNCC,
            this.MoTa});
            this.dataClothes.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataClothes.Location = new System.Drawing.Point(0, 83);
            this.dataClothes.Name = "dataClothes";
            this.dataClothes.ReadOnly = true;
            this.dataClothes.RowTemplate.Height = 24;
            this.dataClothes.Size = new System.Drawing.Size(1316, 513);
            this.dataClothes.TabIndex = 10;
            // 
            // MaHang
            // 
            this.MaHang.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MaHang.DataPropertyName = "MaHang";
            this.MaHang.HeaderText = "Mã Hàng";
            this.MaHang.Name = "MaHang";
            this.MaHang.ReadOnly = true;
            // 
            // TenHang
            // 
            this.TenHang.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TenHang.DataPropertyName = "TenHang";
            this.TenHang.HeaderText = "Tên Hàng";
            this.TenHang.Name = "TenHang";
            this.TenHang.ReadOnly = true;
            // 
            // DonVi
            // 
            this.DonVi.DataPropertyName = "DonVi";
            this.DonVi.HeaderText = "Đơn Vị";
            this.DonVi.Name = "DonVi";
            this.DonVi.ReadOnly = true;
            this.DonVi.Width = 50;
            // 
            // SoLuong
            // 
            this.SoLuong.DataPropertyName = "SoLuong";
            this.SoLuong.HeaderText = "Số Lượng";
            this.SoLuong.Name = "SoLuong";
            this.SoLuong.ReadOnly = true;
            this.SoLuong.Width = 80;
            // 
            // size
            // 
            this.size.DataPropertyName = "size";
            this.size.HeaderText = "Size";
            this.size.Name = "size";
            this.size.ReadOnly = true;
            this.size.Width = 50;
            // 
            // DonGia
            // 
            this.DonGia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.DonGia.DataPropertyName = "DonGia";
            this.DonGia.HeaderText = "Đơn Giá";
            this.DonGia.Name = "DonGia";
            this.DonGia.ReadOnly = true;
            // 
            // GiaXuatBanLe
            // 
            this.GiaXuatBanLe.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GiaXuatBanLe.DataPropertyName = "GiaXuatBanLe";
            this.GiaXuatBanLe.HeaderText = "Giá Xuất Bán Lẻ";
            this.GiaXuatBanLe.Name = "GiaXuatBanLe";
            this.GiaXuatBanLe.ReadOnly = true;
            // 
            // GiaXuatBanSi
            // 
            this.GiaXuatBanSi.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GiaXuatBanSi.DataPropertyName = "GiaXuatBanSi";
            this.GiaXuatBanSi.HeaderText = "Giá Xuất Bán Sỉ";
            this.GiaXuatBanSi.Name = "GiaXuatBanSi";
            this.GiaXuatBanSi.ReadOnly = true;
            // 
            // MaNCC
            // 
            this.MaNCC.DataPropertyName = "MaNCC";
            this.MaNCC.HeaderText = "Nhà cung cấp";
            this.MaNCC.Name = "MaNCC";
            this.MaNCC.ReadOnly = true;
            this.MaNCC.Width = 130;
            // 
            // MoTa
            // 
            this.MoTa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MoTa.DataPropertyName = "MoTa";
            this.MoTa.HeaderText = "Mô Tả";
            this.MoTa.Name = "MoTa";
            this.MoTa.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(163)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1316, 83);
            this.label1.TabIndex = 9;
            this.label1.Text = "Clothes Warehouse";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNhapKho
            // 
            this.btnNhapKho.ActiveBorderThickness = 1;
            this.btnNhapKho.ActiveCornerRadius = 20;
            this.btnNhapKho.ActiveFillColor = System.Drawing.Color.Black;
            this.btnNhapKho.ActiveForecolor = System.Drawing.Color.Magenta;
            this.btnNhapKho.ActiveLineColor = System.Drawing.Color.Magenta;
            this.btnNhapKho.BackColor = System.Drawing.Color.Transparent;
            this.btnNhapKho.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNhapKho.BackgroundImage")));
            this.btnNhapKho.ButtonText = "Warehousing";
            this.btnNhapKho.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNhapKho.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNhapKho.ForeColor = System.Drawing.Color.Transparent;
            this.btnNhapKho.IdleBorderThickness = 1;
            this.btnNhapKho.IdleCornerRadius = 20;
            this.btnNhapKho.IdleFillColor = System.Drawing.Color.Black;
            this.btnNhapKho.IdleForecolor = System.Drawing.Color.White;
            this.btnNhapKho.IdleLineColor = System.Drawing.Color.Yellow;
            this.btnNhapKho.Location = new System.Drawing.Point(240, 654);
            this.btnNhapKho.Margin = new System.Windows.Forms.Padding(5);
            this.btnNhapKho.Name = "btnNhapKho";
            this.btnNhapKho.Size = new System.Drawing.Size(337, 85);
            this.btnNhapKho.TabIndex = 11;
            this.btnNhapKho.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnNhapKho.Click += new System.EventHandler(this.btnNhapKho_Click);
            // 
            // btnReload
            // 
            this.btnReload.ActiveBorderThickness = 1;
            this.btnReload.ActiveCornerRadius = 20;
            this.btnReload.ActiveFillColor = System.Drawing.Color.Black;
            this.btnReload.ActiveForecolor = System.Drawing.Color.Magenta;
            this.btnReload.ActiveLineColor = System.Drawing.Color.Magenta;
            this.btnReload.BackColor = System.Drawing.Color.Transparent;
            this.btnReload.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnReload.BackgroundImage")));
            this.btnReload.ButtonText = "Reload";
            this.btnReload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReload.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReload.ForeColor = System.Drawing.Color.Transparent;
            this.btnReload.IdleBorderThickness = 1;
            this.btnReload.IdleCornerRadius = 20;
            this.btnReload.IdleFillColor = System.Drawing.Color.Black;
            this.btnReload.IdleForecolor = System.Drawing.Color.White;
            this.btnReload.IdleLineColor = System.Drawing.Color.Yellow;
            this.btnReload.Location = new System.Drawing.Point(636, 654);
            this.btnReload.Margin = new System.Windows.Forms.Padding(5);
            this.btnReload.Name = "btnReload";
            this.btnReload.Size = new System.Drawing.Size(337, 85);
            this.btnReload.TabIndex = 12;
            this.btnReload.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnReload.Click += new System.EventHandler(this.btnReload_Click);
            // 
            // UserControl_ClothesWarehouse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.btnReload);
            this.Controls.Add(this.btnNhapKho);
            this.Controls.Add(this.dataClothes);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "UserControl_ClothesWarehouse";
            this.Size = new System.Drawing.Size(1316, 818);
            this.Load += new System.EventHandler(this.UserControl_ClothesWarehouse_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataClothes)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataClothes;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuThinButton2 btnNhapKho;
        private Bunifu.Framework.UI.BunifuThinButton2 btnReload;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn DonVi;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoLuong;
        private System.Windows.Forms.DataGridViewTextBoxColumn size;
        private System.Windows.Forms.DataGridViewTextBoxColumn DonGia;
        private System.Windows.Forms.DataGridViewTextBoxColumn GiaXuatBanLe;
        private System.Windows.Forms.DataGridViewTextBoxColumn GiaXuatBanSi;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaNCC;
        private System.Windows.Forms.DataGridViewTextBoxColumn MoTa;
    }
}
