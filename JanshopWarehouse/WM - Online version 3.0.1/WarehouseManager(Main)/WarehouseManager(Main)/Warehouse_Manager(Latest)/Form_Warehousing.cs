﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Warehouse_Manager_Latest_
{
    public partial class Form_Warehousing : Form
    {
        public Form_Warehousing()
        {
            InitializeComponent();
        }

        SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");

        // bắt đầu viết hàm kết nối tới csdl
        private void KetNoiCSDL()
        {
            try
            {
                //mở kết nối trước:
                conn.Open();
                //tạo chuỗi kết nối:
                string sql = "SELECT *FROM HangHoa";//lấy hết dữ liệu trong bảng hocsinh
                SqlCommand cmd = new SqlCommand(sql, conn);// bắt đầu truy vấn bằng câu lệnh
                cmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                // tạo một kho ảo để lưu trữ dữ liệu
                da.Fill(dt); //đổ dữ liệu vào kho
                             //đóng kết nối
                conn.Close();
                //ta đổ dữ liệu vào trong gridDataView
                dataNhapKho.DataSource = dt;
            }
            catch
            {
                MessageBox.Show("Lỗi kết nối, vui lòng kiểm tra lại");
            }

            finally
            {
                SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
                conn.Close(); //đóng kết nối lại

            }
        }

        public void HienThi()
        {
            string sqlSELECT = "SELECT *FROM HangHoa";
            SqlCommand cmd = new SqlCommand(sqlSELECT, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Load(dr);
            dataNhapKho.DataSource = dt;

        }



        public static void clearTxt(Control ctrl)
        {
            if (ctrl is TextBox)
            {
                ctrl.Text = string.Empty;
            }
            foreach (Control i in ctrl.Controls)
            {
                clearTxt(i);
            }
        }


        private void btnNhapKho_Click(object sender, EventArgs e)
        {
            if (txtMaHang.Text == "")
            {
                baoLoi.SetError(txtMaHang, "Bạn chưa nhập mã hàng");
            }
            if (txtTenHang.Text == "")
            {
                baoLoi.SetError(txtTenHang, "Bạn chưa nhập tên hàng");
            }
            if (txtSoLuong.Text == "")
            {
                baoLoi.SetError(txtSoLuong, "Bạn chưa nhập số lượng");
            }
            if (cbDonVi.Text == "")
            {
                baoLoi.SetError(cbDonVi, "Bạn chưa chọn đơn vị");
            }
            if (cbMaNCC.Text == "")
            {
                baoLoi.SetError(cbMaNCC, "Bạn chưa nhập nhà cung cấp");
            }

            if (txtMaHang.Text != "" && txtTenHang.Text != "" && cbDonVi.Text != "" && cbMaNCC.Text != "")
                try
                {
                    string sqlAdd = "Insert INTO HangHoa VALUES (@MaHang, @TenHang, @DonVi, @size, @SoLuong, @DonGia, @GiaXuatBanLe, @GiaXuatBanSi, @MaNCC, @MoTa)";
                    SqlCommand cmd = new SqlCommand(sqlAdd, conn);
                    cmd.Parameters.AddWithValue("MaHang", txtMaHang.Text);
                    cmd.Parameters.AddWithValue("TenHang", txtTenHang.Text);
                    cmd.Parameters.AddWithValue("DonVi", cbDonVi.Text);
                    cmd.Parameters.AddWithValue("size", txtSize.Text);
                    cmd.Parameters.AddWithValue("SoLuong", txtSoLuong.Text);
                    cmd.Parameters.AddWithValue("DonGia", txtDonGia.Text);
                    cmd.Parameters.AddWithValue("GiaXuatBanLe", txtGiaBanLe.Text);
                    cmd.Parameters.AddWithValue("GiaXuatBanSi", txtGiaBanSi.Text);
                    cmd.Parameters.AddWithValue("MaNCC", cbMaNCC.Text);
                    cmd.Parameters.AddWithValue("MoTa", txtMoTa.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    HienThi();
                    conn.Close();

                    MessageBox.Show("Đã nhập thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }

                catch (Exception ex)
                {
                    if (ex.GetType() == typeof(SqlException))
                    {
                        if (ex.Message.Contains("PRIMARY KEY"))
                        {
                            MessageBox.Show("Bạn đã nhập mã trùng với mã hàng, xin vui lòng nhập mã khác", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        }
                    }
                    else
                    {
                        string sqlAdd = "Insert INTO HangHoa VALUES (@MaHang, @TenHang, @DonVi, @size, @SoLuong, @DonGia, @GiaXuatBanLe, @GiaXuatBanSi, @MaNCC, @MoTa)";
                        SqlCommand cmd = new SqlCommand(sqlAdd, conn);
                        cmd.Parameters.AddWithValue("MaHang", txtMaHang.Text);
                        cmd.Parameters.AddWithValue("TenHang", txtTenHang.Text);
                        cmd.Parameters.AddWithValue("DonVi", cbDonVi.Text);
                        cmd.Parameters.AddWithValue("size", txtSize.Text);
                        cmd.Parameters.AddWithValue("SoLuong", txtSoLuong.Text);
                        cmd.Parameters.AddWithValue("DonGia", txtDonGia.Text);
                        cmd.Parameters.AddWithValue("GiaXuatBanLe", txtGiaBanLe.Text);
                        cmd.Parameters.AddWithValue("GiaXuatBanSi", txtGiaBanSi.Text);
                        cmd.Parameters.AddWithValue("MaNCC", cbMaNCC.Text);
                        cmd.Parameters.AddWithValue("MoTa", txtMoTa.Text);
                        cmd.ExecuteNonQuery();
                        HienThi();
                        conn.Close();

                        MessageBox.Show("Đã nhập thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        //throw ex;
                    }
                }
                finally
                {
                    SqlConnection conn = new SqlConnection(@"Server=tcp:warehousemanager.database.windows.net,1433;Initial Catalog=WarehouseManager(Database);Persist Security Info=False;User ID=letrunghieu8;Password=Letrunghieu1997;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;");
                    conn.Close(); //đóng kết nối lại

                }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Hide();

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            clearTxt(this);
        }
        // thêm 1 hàm load dữ liệu lên các textBox

        private void loadData()
        {
            txtMaHang.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtMaHang.DataBindings.Add("Text", dataNhapKho.DataSource, "MaHang");
            txtTenHang.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtTenHang.DataBindings.Add("Text", dataNhapKho.DataSource, "TenHang");
            cbDonVi.DataBindings.Clear();//xóa dữ liệu trong textbox
            cbDonVi.DataBindings.Add("Text", dataNhapKho.DataSource, "DonVi");
            txtSize.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtSize.DataBindings.Add("Text", dataNhapKho.DataSource, "size");
            txtSoLuong.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtSoLuong.DataBindings.Add("Text", dataNhapKho.DataSource, "SoLuong");
            txtDonGia.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtDonGia.DataBindings.Add("Text", dataNhapKho.DataSource, "DonGia");
            txtGiaBanLe.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtGiaBanLe.DataBindings.Add("Text", dataNhapKho.DataSource, "GiaXuatBanLe");
            txtGiaBanSi.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtGiaBanSi.DataBindings.Add("Text", dataNhapKho.DataSource, "GiaXuatBanSi");
            cbMaNCC.DataBindings.Clear();//xóa dữ liệu trong textbox
            cbMaNCC.DataBindings.Add("Text", dataNhapKho.DataSource, "MaNCC");
            txtMoTa.DataBindings.Clear();//xóa dữ liệu trong textbox
            txtMoTa.DataBindings.Add("Text", dataNhapKho.DataSource, "MoTa");

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult thongBao;
            thongBao = MessageBox.Show("Are You Sure?", "Thông Báo", MessageBoxButtons.YesNo, MessageBoxIcon.Stop);
            if (thongBao == DialogResult.Yes)
            {
                try
                {
                    string sqlDELETE = "DELETE FROM HangHoa Where MaHang = @MaHang";
                    SqlCommand cmd = new SqlCommand(sqlDELETE, conn);
                    cmd.Parameters.AddWithValue("MaHang", txtMaHang.Text);
                    cmd.Parameters.AddWithValue("TenHang", txtTenHang.Text);
                    cmd.Parameters.AddWithValue("DonVi", cbDonVi.Text);
                    cmd.Parameters.AddWithValue("size", txtSize.Text);
                    cmd.Parameters.AddWithValue("SoLuong", txtSoLuong.Text);
                    cmd.Parameters.AddWithValue("DonGia", txtDonGia.Text);
                    cmd.Parameters.AddWithValue("GiaXuatBanLe", txtGiaBanLe.Text);
                    cmd.Parameters.AddWithValue("GiaXuatBanSi", txtGiaBanSi.Text);
                    cmd.Parameters.AddWithValue("MoTa", txtMoTa.Text);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    HienThi();
                    conn.Close();
                    loadData();

                    MessageBox.Show("Đã xóa thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }
                catch
                {
                    MessageBox.Show("Something went wrong");
                }
            }
        }

        private void Form_Warehousing_Load(object sender, EventArgs e)
        {
            KetNoiCSDL();
            loadData();
            layMaNCC();
        }

        //combobox Mã nhà cung cấp
        public void layMaNCC()
        {
            KetnoiDatabase kn = new KetnoiDatabase();
            SqlCommand mysqlcommand = new SqlCommand();
            mysqlcommand.Connection = kn.kn;
            mysqlcommand.CommandText = "select nhaCC from NhaCungCap";
            kn.kn_csdl();
            SqlDataAdapter mysqladatareader = new SqlDataAdapter();
            mysqladatareader.SelectCommand = mysqlcommand;
            DataSet mydataset = new DataSet();
            mysqladatareader.Fill(mydataset, "KQ");
            DataTable table_MK = new DataTable();
            table_MK = mydataset.Tables["KQ"];
            cbMaNCC.DataSource = table_MK;
            cbMaNCC.DisplayMember = table_MK.Columns["NhaCC"].ToString();



            kn.kn.Close();

        }

        private void dataNhapKho_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                dataNhapKho.CurrentRow.Selected = true; // dữ liệu đc chọn cả dòng
            }

            catch
            {
            }
        }



        private void txtSoLuong_TextChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(txtSoLuong, "");
        }

        private void txtMaHang_TextChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(txtMaHang, "");
        }

        private void txtTenHang_TextChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(txtTenHang, "");
        }

        private void cbDonVi_SelectedIndexChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(cbDonVi, "");
        }

        private void cbMaNCC_SelectedIndexChanged(object sender, EventArgs e)
        {
            baoLoi.SetError(cbMaNCC, "");
        }

    }
}
